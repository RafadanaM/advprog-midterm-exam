package id.ac.ui.cs.advprog.midterm.domain;

import org.junit.jupiter.api.Test;
import static org.junit.Assert.assertEquals;

public class UserTest {

        private User user;



        @Test
        public void setNameTest() {
            User user = new User();
            user.setName("Rafadana");
            assertEquals("Rafadana", user.getName());

        }

        @Test
        public void setIdTest() {
            User user = new User();
            Long newId = 12345L;
            user.setId(newId);
            assertEquals(12345L, user.getId());

        }

        @Test
        public void setEmailTest() {
            User user = new User();
            user.setEmail("email@email.com");
            assertEquals("email@email.com", user.getEmail());

        }


        @Test
        public void toStringTest() {
            User user = new User();
            user.setId(12345);
            assertEquals("User{id=12345}", user.toString());

        }

}
