package id.ac.ui.cs.advprog.midterm.controller;

import id.ac.ui.cs.advprog.midterm.domain.User;
import id.ac.ui.cs.advprog.midterm.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class userControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Mock
    private BindingResult result;

    @Mock
    private Model model;

    @MockBean
    private UserRepository userRepository;

    private User user = new User();


    @BeforeEach
    void setUp() {
        user = new User();
        user.setId(1);
        user.setName("Rafadana");
        user.setEmail("email@email.com");
        when(userRepository.findById(user.getId()))
                .thenReturn(java.util.Optional.ofNullable(user));

    }



    @Test
    public void showSignUpFormTest() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.get("/signup"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(view().name("add-user"));
    }

    @Test
    public void addUserValidTemplate() throws Exception {
        mockMvc.perform(post("/adduser")
                .flashAttr("user", user)
                .flashAttr("result", result)
                .flashAttr("model", model))
                .andExpect(status().isOk())
                .andExpect(view().name("index"));


    }

    @Test
    public void addUserInvalidTemplate() throws Exception {
        user.setName(null);

        mockMvc.perform(post("/adduser")
                .flashAttr("user", user)
                .flashAttr("result", result)
                .flashAttr("model", model))
                .andExpect(status().isOk())
                .andExpect(view().name("add-user"));
    }

    @Test
    public void editValidTemplate() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.get("/edit/" + user.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(view().name("update-user"))
                .andExpect(model().attributeExists("user"));

    }

    @Test
    public void editInvalidTemplate() throws Exception {

        try {
            mockMvc.perform(MockMvcRequestBuilders.get("/edit/123"))
                    .andExpect(MockMvcResultMatchers.status().isOk())
                    .andExpect(view().name("update-user"))
                    .andExpect(model().attributeExists("user"));
        } catch (Exception e) {
            assertEquals("Request processing failed; nested exception is java.lang.IllegalArgumentException: Invalid user Id:123", e.getMessage());
        }


    }

    @Test
    public void updateValidTemplate() throws Exception {

        mockMvc.perform(post("/update/" + user.getId())
                .flashAttr("user", user)
                .flashAttr("result", result)
                .flashAttr("model", model))
                .andExpect(status().isOk())
                .andExpect(view().name("index"));

    }

    @Test
    public void updateInvalidTemplate() throws Exception {
        user.setName(null);
        mockMvc.perform(post("/update/" + user.getId())
                .flashAttr("user", user)
                .flashAttr("result", result)
                .flashAttr("model", model))
                .andExpect(status().isOk())
                .andExpect(view().name("update-user"));

    }


    @Test
    public void deleteValidTemplate() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.get("/delete/" + user.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(view().name("index"))
                .andExpect(model().attributeDoesNotExist("user"));

    }

    @Test
    public void deleteInValidTemplate() throws Exception {

        try {
            mockMvc.perform(MockMvcRequestBuilders.get("/delete/12345"))
                    .andExpect(MockMvcResultMatchers.status().isOk())
                    .andExpect(view().name("index"))
                    .andExpect(model().attributeDoesNotExist("user"));
        } catch (Exception e) {
            assertEquals("Request processing failed; nested exception is java.lang.IllegalArgumentException: Invalid user Id:12345", e.getMessage());
        }


    }

    @Test
    public void countTest() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.get("/count"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(view().name("count"));
    }








}
